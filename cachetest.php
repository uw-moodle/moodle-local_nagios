<?php
/*
 * Nagios check for moodle cache consistency
 *
 * This script checks several css and js files in the backend cache
 * to ensure that that are nonempty and recent.
 *
 * @author Matt Petro
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('nagios_check.class.php');

class local_nagios_check_cache extends local_nagios_check {

    protected function _do_check() {
        global $DB, $CFG;

        $themename = $CFG->theme;
        $themecachedir = $CFG->cachedir.'/theme/'.$themename;
        $jscachedir = $CFG->cachedir.'/js';

        // Array of what files to check
        // We can't check every file in the cache directory since some themes will have empty js files.
        $checkfiles = array($themecachedir.'/css/all.css',
                            $themecachedir.'/css/parents.css',
                            $themecachedir.'/css/plugins.css',
                            $themecachedir.'/css/theme.css',
                            $themecachedir.'/javascript_footer.js',
                            $themecachedir.'/javascript_head.js',
        );

        // Add all JS files stored by etag.  None of these should be empty.
        $jsfiles = get_directory_list($jscachedir, '', false);
        foreach ($jsfiles as $jsfile) {
            $checkfiles[] = $jscachedir.'/'.$jsfile;
        }

        $success = true;
        $description = '';

        $lastpurge = get_config('local_uwmoodle', 'lastcachepurge');
        if (!$lastpurge) {
            echo "No last cache purge time.  Not comparing cache timestamps.\n";
        }

        foreach ($checkfiles as $filename) {
            if (!file_exists($filename)) {
                echo "Ignoring missing file: $filename\n";
                continue;
            }
            $info = stat($filename);
            if (!$info) {
                $success = false;
                $description .= "Cannot stat: $filename\n";
                continue;
            }
            $size = $info[7];
            $mtime = $info[9];
            if ($size == 0) {
                $success = false;
                $description .= "Empty cache file: $filename\n";
            }
            if ($lastpurge && $mtime + 30 < $lastpurge) {  // with grace period
                $success = false;
                $description .= "Old cache file: $filename (" . ($lastpurge-$mtime) ." seconds too old)\n";
            }
        }

        if ($success) {
            $result =  array(self::OK, "Cache Test OK");
        } else {
            echo "Test Failed\n";
            $result =  array(self::CRITICAL, $description);
        }

        return $result;
    }
}


$testclass = new local_nagios_check_cache();
$testclass->setup_page();
echo $testclass->run_check();