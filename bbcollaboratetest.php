<?php
/*
 * Test blackboard collaborate connection
 *
 * This script is mostly copied from mod/elluminate/conntest.php
 *
 * @author Matt Petro
 */

define('AJAX_SCRIPT', true);
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('nagios_check.class.php');

class local_nagios_check_bbcollaborate extends local_nagios_check {
    public function get_name() {
        return 'bbcollaborate';
    }

    protected function _do_check() {
        global $CFG;

        echo "Testing Blackboard Collaborate \n";

        $include = $CFG->dirroot.'/mod/elluminate/lib.php';
        if (!file_exists($include)) {
            throw new local_nagios_exception("Cannot include elluminate/lib.php", self::WARNING);
        }
        require_once($CFG->dirroot.'/mod/elluminate/lib.php');

        $serverurl = $CFG -> elluminate_server;
        $username = $CFG -> elluminate_auth_username;
        $password = $CFG -> elluminate_auth_password;

        if (!class_exists('Elluminate_WS_SchedulingManagerFactory')) {
            throw new local_nagios_exception("Elluminate_WS_SchedulingManagerFactory class doesn't exist", self::WARNING);
        }

        $schedManager = Elluminate_WS_SchedulingManagerFactory::getSchedulingManagerWithSettings($serverurl, $username, $password);

        if (!method_exists($schedManager, 'testConnection')) {
            throw new local_nagios_exception("testConnection() method doesn't exist", self::WARNING);
        }

        if (!$schedManager->testConnection()) {
            throw new local_nagios_exception("Connection test failed", self::CRITICAL);
        }

        return array(self::OK, "Blackboard Collaborate Test OK");
    }
}

$testclass = new local_nagios_check_bbcollaborate();
$testclass->setup_page();
echo $testclass->run_check();