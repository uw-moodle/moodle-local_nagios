<?php
/*
 * Nagios check for course backups.
 *
 * This script checks for course backups which failed on the last automated backup run, as well
 * as course backups which have been scheduled to run for more than a configured number of seconds.
 *
 * @author Matt Petro
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/backup/util/helper/backup_cron_helper.class.php');
require_once('nagios_check.class.php');

class local_nagios_check_coursebackups extends local_nagios_check {

    const TOO_OLD = 86400; // Look for backups which have been pending for more than 1 day

    protected function _do_check() {
        global $DB, $CFG;

        $success = true;
        $description = '';

        $now = time();

        if (get_config('backup', 'backup_auto_active') == 0) {
            // Automatic backups disabled
            echo "Automated backups disabled";
            return array(self::OK, "Course Backups disabled permanently.");
        }

        // Check for pending course backups
        $recs = $DB->get_records_select('backup_courses', 'nextstarttime > 0 AND nextstarttime < :time', array('time' => $now - static::TOO_OLD));
        if (!empty($recs)) {
            $description .= " Some course backups have been pending for > ".static::TOO_OLD." seconds.";
            echo "PENDING BACKUPS: \n";
            foreach ($recs as $rec) {
                $coursename = $DB->get_field('course', 'fullname', array('id'=>$rec->courseid));
                echo " Course id $rec->courseid : $coursename \n";
            }
            $success = false;
        }

        // Check for errored course backups
        $recs = $DB->get_records_select('backup_courses', 'laststatus = '.backup_cron_automated_helper::BACKUP_STATUS_ERROR.' AND lastendtime > 0');
        if (!empty($recs)) {
            $description .= " Some course backups have errored state.";
            echo "ERRORED BACKUPS: \n";
            foreach ($recs as $rec) {
                $coursename = $DB->get_field('course', 'fullname', array('id'=>$rec->courseid));
                echo " Course id $rec->courseid : $coursename\n";
            }
            $success = false;
        }

        // Count the number of backups that ran in the last 3*$too_old seconds.
        // This number is a little arbitrary, but we don't want to trigger if nobody changes anything for a day or two.
        // The SITE course will always backup if someone has logged into the site.
        $max_too_old = 3*static::TOO_OLD;
        $backupcount = $DB->count_records_select('backup_courses', 'lastendtime > :time', array('time' => $now-$max_too_old));
        if ($backupcount == 0) {
            $description .= " No course has been backed up in $max_too_old seconds.  This could be due to site inactivity, or backups not running.";
            $success = false;
        } else {
            echo "There were $backupcount courses backed up in the last $max_too_old seconds.  OK.\n";
        }

        if ($success) {
            $result =  array(self::OK, "Course Backup Test OK");
        } else {
            echo "Test Failed\n";
            echo "For more information see the moodle backup report: ".$CFG->wwwroot."/report/backups/index.php";
            $result =  array(self::CRITICAL, $description);
        }

        return $result;
    }
}


$testclass = new local_nagios_check_coursebackups();
$testclass->setup_page();
echo $testclass->run_check();