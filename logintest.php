<?php
/*
 * Test moodle logins
 *
 * This script uses CURL to login to moodle with the user defined in the credentials file.
 *
 * @author Matt Petro
 */

define('AJAX_SCRIPT', true);
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('nagios_check.class.php');

class local_nagios_check_logintest extends local_nagios_check {

    protected function _do_check() {
        global $CFG;

        echo "Testing Moodle Login \n";

        if (!(include 'credentials.php')) {
            throw new local_nagios_exception('Error: Can\'t open: '.dirname(__FILE__).'/credentials.php');
        }

        $cookiefile = tempnam ("/tmp", "Nagios");

        try{
            echo "Starting trace...";
            $this->login_test($cookiefile, 45);
        } catch( Exception $e ) {
           unlink($cookiefile);
           throw $e;
        }
        unlink($cookiefile);

        return array(self::OK, "Login Test OK");
    }

    protected function login_test($cookiefile, $timeout) {
        global $CFG;

        $starttime = time();
        $postvars = array('username'=>MOODLEUSER,'password'=>MOODLEPASS, 'submit'=>'Login');
        $loginurl = $CFG->wwwroot.'/login/index.php';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
        curl_setopt($ch, CURLOPT_URL, $loginurl);
        $content = curl_exec( $ch );
        echo "URL: $loginurl timestamp: ".(time()-$starttime)."s \n";
        if(!$response = curl_getinfo( $ch )){
            throw new Exception("FAILED URL: $loginurl");
        }

        if ($response['http_code'] != 200){
            throw new Exception("FAILED URL: $loginurl, response code: ".$response['http_code']);
        }

        if (!preg_match('/You are logged in/', $content)) {
            throw new Exception("LOGIN FAILED");
        }

        // logout so we don't keep making sessions
        $matches = array();
        if (!preg_match('/sesskey=[\w]+/', $content, $matches)) {
            throw new Exception("NO SESSKEY FOUND");
        }
        $sesskeyparam = $matches[0];
        $logouturl = $CFG->wwwroot.'/login/logout.php?'.$sesskeyparam;

        curl_setopt($ch, CURLOPT_URL, $logouturl);
        curl_setopt($ch, CURLOPT_POST, false);

        $content = curl_exec( $ch );
        echo "URL: ".strstr($logouturl,'?',true)." timestamp: ".(time()-$starttime)."s \n";
        if(!$response = curl_getinfo( $ch )){
            throw new Exception("FAILED URL: $logouturl");
        }
        if ($response['http_code'] != 200){
            throw new Exception("FAILED URL: $logouturl, response code: ".$response['http_code']);
        }

        curl_close ( $ch );

        return true;
    }

}

$testclass = new local_nagios_check_logintest();
$testclass->setup_page();
echo $testclass->run_check();
