<?php
/**
 * Return server memory status.
 */

header('Content-Type:text/plain; charset=ISO-8859-15');

$fh = fopen('/proc/meminfo', 'r');
$M = array();
while ($line = fgets($fh)) {
	$pieces = array();
	if (preg_match('/^(.*):\s*(\d+)/', $line, $pieces)) {
		$M[$pieces[1]] = $pieces[2];
	}
}

echo "NODE: ".`hostname`;
echo "MEM: ". ($M['MemTotal'] - ($M['MemFree'] + $M['Buffers'] + $M['Cached']))."\n";
echo "MEMTOTAL: ".$M['MemTotal']."\n";
echo "SWAP: ". ($M['SwapTotal']-$M['SwapFree'])."\n";
echo "SWAPTOTAL: ".$M['SwapTotal']."\n";


