<?php
/**
 * Nagios check for the course dashboard remote query.
 *
 * This check requires config.php to be loaded, so it will fail when the moodle instance isn't functioning.
 *
 * @author Matt Petro
 */

define('AJAX_SCRIPT', true);
require_once('nagios_check.class.php');

class local_nagios_check_coursedashboard extends local_nagios_check {

    // Time in seconds which will result in WARNING or CRITICAL states.
    // The course dashboard directly affects perceived performance on the "my" page, so these are set pretty low.
    const WARNING_TIME  = 4;
    const CRITICAL_TIME = 8;

    protected function _do_check() {
        global $DB, $CFG;

        if (!(include 'credentials.php')) {
            throw new local_nagios_exception('Error: Can\'t open: '.dirname(__FILE__).'/credentials.php');
        }

        $success = true;
        $description = '';

        $eppn = COURSEDASHBOARDNETID . '@wisc.edu';
        echo "Getting external courses for $eppn...\n";

        require_once(dirname(dirname(dirname(__FILE__))).'/config.php');

        require_once($CFG->dirroot . '/enrol/wisc/lib/externalcourses/external_user_courses.php');
        $externalcourses = new external_user_courses();
        $ecourses = $externalcourses->get_user_courses($eppn, array(), true);
        echo "Received " . count($ecourses) . " courses.\n";

        $errors = $externalcourses->get_errors();
        if (!empty($errors)) {
            // Don't want to reveal tokens, so don't show the errors messages themselves.
            $description = "Error querying ".count($errors). " sites.  Login in as an admin in moodle to see the errors.";
            $success = false;
        }

        if ($success) {
            $result =  array(self::OK, "Course dashboard test OK");
        } else {
            echo "Course dashboard test Failed\n";
            $result =  array(self::CRITICAL, $description);
        }

        return $result;
    }
}

$testclass = new local_nagios_check_coursedashboard();
$testclass->setup_page();
echo $testclass->run_check();