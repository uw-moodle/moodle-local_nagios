<?php
/*
 * Nagios check for cron.
 *
 * This script checks that cron has run recently.
 *
 * @author Matt Petro
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('nagios_check.class.php');

class local_nagios_check_crontest extends local_nagios_check {

    const TOO_OLD = 7200; // check that it ran in the last 2 hours

    protected function _do_check() {
        global $DB, $CFG;

        $success = true;
        $description = '';

        $now = time();

        $lastcron = $DB->get_field_sql('SELECT MAX(lastcron) FROM {modules}');
        $cronoverdue = ($lastcron < time() - static::TOO_OLD);
        if ($cronoverdue) {
            echo "Cron last run: ".date("r", $lastcron)."\n";
            $success = false;
        }

        if ($success) {
            $result =  array(self::OK, "Cron test OK");
        } else {
            echo "Cron test Failed\n";
            $result =  array(self::CRITICAL, $description);
        }

        return $result;
    }
}


$testclass = new local_nagios_check_crontest();
$testclass->setup_page();
echo $testclass->run_check();