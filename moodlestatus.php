<?php
/**
 * Return the number of active users on the site
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');

header('Content-Type:text/plain; charset=ISO-8859-15');

$timetoshowusers = 300;
$timefrom = time()-$timetoshowusers;

$csql = "SELECT COUNT(u.id)
   FROM {user} u
   WHERE u.lastaccess > :timefrom";

$usercount = $DB->count_records_sql($csql, array('timefrom'=>$timefrom));

echo "USERS: $usercount\n";
