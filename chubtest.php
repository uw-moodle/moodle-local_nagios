<?php
/**
 * Nagios check for the DoIT CHUB webservice
 *
 * @author Matt Petro
 */

require_once(dirname(dirname(dirname(__FILE__))).'/local/wiscservices/classes/local/soap/wss_soapclient.php');
use local_wiscservices\local\soap\wss_soapclient;


require_once('nagios_check.class.php');

class local_nagios_check_chub extends local_nagios_check {

    protected function _do_check() {
        if (!(include 'credentials.php')) {
            throw new local_nagios_exception('Error: Can\'t open: '.dirname(__FILE__).'/credentials.php');
        }

        echo "Opening datastore\n";

        $options = array(
                'exceptions' => true,
                'trace' => true,
                'connection_timeout' => 20,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
        );

        $client = new wss_soapclient(CHUBURL, $options);
        $client->__setUsernameToken(CHUBUSER, CHUBPASS);

        echo "Reading terms...\n";
        $terms = $client->GetAvailableTerms();
        $terms = $terms->term;

        if (count($terms) == 0) {
            throw new local_nagios_exception( 'No terms returned from CHUB' , self::WARNING);
        }
        echo "got ".count($terms)." terms\n";

        foreach( $terms as $term ) {
            echo "Term: $term->longDescription ($term->termCode)\n";
            echo "  Reading departments... ";
            $params = array('termCode'=>$term->termCode);
            $depts = $client->GetSubjectsInTerm($params);
            if(!empty($depts->subject)){
                $depts = $depts->subject;
                echo "got ".count($depts)." depts\n";
            }
        }
        return array(self::OK, "CHUB Test OK");
    }
}

/* Need this Moodle function to be able to use all the functionality of wss_soapclient class */
function microtime_diff($a, $b) {
    list($a_dec, $a_sec) = explode(' ', $a);
    list($b_dec, $b_sec) = explode(' ', $b);
    return $b_sec - $a_sec + $b_dec - $a_dec;
}


$testclass = new local_nagios_check_chub();
$testclass->setup_page();
echo $testclass->run_check();
