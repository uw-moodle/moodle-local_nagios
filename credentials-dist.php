<?php
// This is the credentials configuration page for Nagios tests.
// All definitions should be filled and the file renamed credentials.php

define('NETID','');        //Test NETID to query from DOIT

/* People Picker */
define('PPURL','');        //Server URL
define('PPUSER','');       //Username
define('PPPASS','');       //Password
define('PPNETID',NETID);   //Netid to query

/* getUDSPerson */
define('UDSURL','');       //Server URL
define('UDSUSER','');      //Username
define('UDSPASS','');      //Password
define('UDSNETID',NETID);  //Netid to query

/* Course dashboard */
define('COURSEDASHBOARDNETID',NETID);

/* CHUB */
define('CHUBURL','');      //Server URL
define('CHUBUSER', '');    //Username
define('CHUBPASS', '');    //Password

/* Moodle (manual) login */
define('MOODLEUSER','');   //Username
define('MOODLEPASS','');   //Password

/* NetID login */
define('NETIDUSER','');    //NetID
define('NETIDPASS','');    //Password

/* Repository tests */
define('REPOTESTUSER','');   //Moodle username for testing repositories
?>