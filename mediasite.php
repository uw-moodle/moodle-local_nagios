<?php
/*
 * Nagios check for mediasite.
 *
 * This script runs a search against engineering mediasite.
 *
 * @author Matt Petro
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('nagios_check.class.php');

require_once(dirname(__FILE__) . '/../../config.php');
require_once("$CFG->dirroot/mod/mediasite/locallib.php");

class local_nagios_check_mediasite extends local_nagios_check {

    // Time in seconds which will result in WARNING or CRITICAL states.
    const WARNING_TIME  = 30;
    const CRITICAL_TIME = 45;

    /**
     * Search code mostly copied from mediasitesearch.php
     * @see local_nagios_check::_do_check()
     */
    protected function _do_check() {
        global $DB;
        $success = self::OK;
        $description = '';

	srand(time());

        $defaults = $DB->get_record('mediasite_config', array());
        $client = mediasite_client($defaults->siteid);
        if (!$client) {
            $description .= 'Unable to create mediasite client.';
            $description .= 'Please ensure that the mediasite server is configured and then actually submit the relevant forms. (This is important.)';
            $success = self::CRITICAL;
        }

        if ($success != self::CRITICAL) {
            $this->search($defaults->siteid, "ECE", "Catalog", $success, $description);
            $this->search($defaults->siteid, "test", "Presentation", $success, $description);
        }

        $result =  array($success, $description);
        return $result;
    }

    /**
     * Search code mostly copied from mediasitesearch.php
     * @see local_nagios_check::_do_check()
     */
    protected function search($siteid, $searchText, $resourceType, &$success, &$description) {

        $searchoptions = new Sonicfoundry\SearchOptions();
        $searchoptions->Session = rand(1,1000000);
        $searchoptions->Course = 1;
        $searchoptions->Site = $siteid;

        $searchoptions->SearchText = urlencode($searchText);
        $searchoptions->ResourceType = $resourceType;
        $searchoptions->After = false;
        $searchoptions->AfterDate = null;
        $searchoptions->Until = false;
        $searchoptions->UntilDate = null;
        $searchoptions->Name = true;
        $searchoptions->Description = true;
        $searchoptions->Tag = true;
        $searchoptions->Presenter = false;

        $results = array();
        $total = -1;
        $description .= 'Querying '.$searchoptions->ResourceType.'"'.$searchoptions->SearchText.'"... ';
        try {
            $truncated = mediasite_search($siteid, $searchoptions, $results, $total);

            if(count($results) == 0) {
                if ($success != self::CRITICAL) {
                    $success = self::WARNING;
                }
                $description .= 'No search results returned. ';
            } else {
                $description .= 'Got '.count($results).' results. ';
            }
        } catch (Exception $e) {
            $success = self::CRITICAL;
            $description .= $e->getMessage();
        }
    }


}

$testclass = new local_nagios_check_mediasite();
$testclass->setup_page();
echo $testclass->run_check();
