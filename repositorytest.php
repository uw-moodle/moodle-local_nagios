<?php
/*
 * Test repository plugins
 *
 * This script tests all repositories available to the user 'REPOTESTUSER' by requesting
 * a directory listing for each repository type.  In particular it's useful for testing remote repositories.
 * For OAUTH repositories (google docs, etc) the REPOTESTUSER should have login tokens already
 * established.
 *
 * REPOTESTUSER is defined in the configuration file.
 *
 * @author Matt Petro
 */

// Important - don't pass the nagios login cookie back to the browser
define('NO_MOODLE_COOKIES', true);

define('AJAX_SCRIPT', true);
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('nagios_check.class.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->dirroot.'/repository/lib.php');
require_once($CFG->libdir.'/googleapi.php');

class local_nagios_check_repositorytest extends local_nagios_check {

    var $type = null;

    public function __construct($type) {
        $this->type = $type;
    }

    protected function _do_check() {
        global $CFG, $PAGE, $DB;

        if (!$this->type) {
            throw new local_nagios_exception('Must specify repository parameter: ?type={dropbox|boxnet|googledocs|wikimedia}', self::UNKNOWN);
        }
        if (!(include 'credentials.php')) {
            throw new local_nagios_exception('Error: Can\'t open: '.dirname(__FILE__).'/credentials.php');
        }

        echo "Testing Repository \n";

        $PAGE->set_context(context_system::instance());

        echo "Checking repositories for user \"".REPOTESTUSER."\" \n";
        $user = $DB->get_record('user', array('username'=>REPOTESTUSER, 'deleted'=>0, 'mnethostid'=>$CFG->mnet_localhost_id), '*', MUST_EXIST);

        \core\session\manager::set_user($user);

        $context = context_user::instance($user->id);

        echo "Getting repository instance \n";

        $params = array();
        $params['context'] = array($context, get_system_context());
        $params['currentcontext'] = $context;
        $params['return_types'] = FILE_INTERNAL;
        $params['type'] = $this->type;
        $instances = repository::get_instances($params);
        $instance = reset($instances);

        if (!$instance) {
            throw new local_nagios_exception("Repository type $this->type not enabled", self::WARNING);
        }
        $repooptions = array(
                'ajax' => false,
                'mimetypes' => '*',
        );
        $info = $instance->get_meta();
        try {
            $repo = repository::get_repository_by_id($info->id, $context->id, $repooptions);
            echo "Checking {$info->name}: ";
            switch ($info->type) {
                case 'dropbox':
                case 'boxnet':
                    // Moodle doesn't store an oauth2 refresh token, so this is hard to test directly.
                    // Instead, we just fetch the API url directly and look for a 40X error.
                    // Here we're really testing for network connectivity only.
                    // Initialize the curl object
                    $url = BOXNET_CLIENT::API;
                    $curl = curl_init($url);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec($curl);

                    $info = curl_getinfo( $curl );

                    // We're expecting a 40X error :)
                    if ($info['http_code'] == 400 || $info['http_code'] == 401) {
                        echo " Success.";
                    } else {
                        throw new local_nagios_exception(" FAILED URL: $url, response code: ".$info['http_code'], self::CRITICAL);
                    }
                    curl_close($curl);
                    break;

                case 'googledocs':
                    // Moodle doesn't store an oauth2 refresh token, so this is hard to test directly.
                    // Instead, we just fetch the API url directly and look for a 400 error.
                    // Here we're really testing for network connectivity only.

                    // Initialize the curl object
                    $url = google_docs::DOCUMENTFEED_URL;
                    $curl = curl_init($url);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec($curl);

                    $info = curl_getinfo( $curl );

                    // We're expecting a 400 error :)
                    if ($info['http_code'] == 400) {
                        echo " Success.";
                    } else {
                        throw new local_nagios_exception(" FAILED URL: $url, response code: ".$info['http_code'], self::CRITICAL);
                    }
                    curl_close($curl);
                    break;

                case 'wikimedia':
                    // Search for something and see if we get a response
                    $search_result = $repo->search('bucky');
                    $count = count($search_result['list']);
                    if (!empty($search_result['list'])) {
                        echo " Success: Got $count search results.";
                    } else {
                        throw new local_nagios_exception('No search results', self::WARNING);
                    }
                    break;
                default:
                    throw new local_nagios_exception('No repository test defined for this type.', self::UNKNOWN);
                    ;
            }
        } catch (Exception $e) {
            // clean up the session
            \core\session\manager::terminate_current();
            throw $e;
        }

        // clean up the session
        \core\session\manager::terminate_current();
        return array(self::OK, "Repository Test OK");
    }
}

$type = optional_param('type', null, PARAM_ALPHAEXT);

$testclass = new local_nagios_check_repositorytest($type);
$testclass->setup_page();
echo $testclass->run_check();
