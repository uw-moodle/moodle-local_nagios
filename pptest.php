<?php
/**
 * Nagios check for the DoIT getIMSPeople() webservice
 *
 * @author Matt Petro
 */

require_once(dirname(dirname(dirname(__FILE__))).'/local/wiscservices/classes/local/soap/wss_soapclient.php');
use local_wiscservices\local\soap\wss_soapclient;

define('AJAX_SCRIPT', true);
require_once('nagios_check.class.php');

class local_nagios_check_peoplepicker extends local_nagios_check {

    protected function _do_check() {
        if (!(include 'credentials.php')) {
            throw new local_nagios_exception('Error: Can\'t open: '.dirname(__FILE__).'/credentials.php');
        }

        echo "Opening peoplepicker \n";

        $options = array(
            'exceptions' => true,
             'trace' => true,
             'connection_timeout' => 20,
             'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
        );

        $client = new wss_soapclient(PPURL, $options);
        $client->__setUsernameToken(PPUSER, PPPASS);

        echo "Calling getPeopleByNetid...\n";
        $params = array('People' => array('sourcedid' => array(array("source" => 'UW-Madison NetID', "id" => PPNETID))));
        $people = $client->getIMSPeople($params);
        if (count($people) != 1) {
            throw new Exception("Got no result for netid '".PPNETID."'.");
        }

        return array(self::OK, "Peoplepicker Test OK");
    }
}

/* Need this Moodle function to be able to use all the functionality of wss_soapclient class */
function microtime_diff($a, $b) {
    list($a_dec, $a_sec) = explode(' ', $a);
    list($b_dec, $b_sec) = explode(' ', $b);
    return $b_sec - $a_sec + $b_dec - $a_dec;
}


$testclass = new local_nagios_check_peoplepicker();
$testclass->setup_page();
echo $testclass->run_check();