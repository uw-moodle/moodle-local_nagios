<?php

/**
 * Base class for UW Moodle nagios checks
 *
 * @author petro
 *
 */
class local_nagios_check {

    // Time in seconds which will result in WARNING or CRITICAL states.
    // Can be overridden by individual checks
    const WARNING_TIME  = 5;
    const CRITICAL_TIME = 15;

    // Status messages (final)
    const UNKNOWN  = "UNKNOWN";
    const CRITICAL = "CRITICAL";
    const WARNING  = "WARNING";
    const OK       = "OK";

    // Run the actual check.  This must be overridden.
    protected function _do_check() {
        throw new Exception( 'Coding error: _do_check() method not implemented' );
    }

    public function setup_page() {
        header('Content-Type: text/xml');
    }

    public function run_check() {
        $timestart  = time();
        ob_start();

        $status = self::UNKNOWN;
        $description = "";

        try {
            list ($status, $description) = $this->_do_check();
            $status = self::normalize_status($status);
        } catch( local_nagios_exception $e ) {
            $status = $e->getStatus();
            $description = $e->getMessage();
        } catch ( Exception $e ) {
            $status = self::CRITICAL;
            $description = $e->getMessage();
        }

        $output = ob_get_clean();
        $elapsedtime = time() - $timestart;

        $output .= "\n --- Elapsed time: $elapsedtime s\n";

        // See if the check too too long.
        if ($status !== self::UNKNOWN) {
            if ($elapsedtime >= static::CRITICAL_TIME) {
                $status = self::CRITICAL;
                $description .= " -- Check took $elapsedtime seconds";
            }
            else if ($elapsedtime >= static::WARNING_TIME) {
                if ($status !== self::CRITICAL) {
                    $status = self::WARNING;
                }
                $description .= " -- Check took $elapsedtime seconds";
            }
        }
        // This can be removed once the nagios check commands are rewritten.  For now,
        // make sure ERROR is somewhere in the output if the test is CRITICAL.
        if ($status === self::CRITICAL) {
            $output .= ' (status: ERROR)';
        }
        return $this->xml_output($status, $description, $output);
    }

    protected function xml_output($status, $description, $output) {
        $out=new XMLWriter();
        $out->openMemory();
        $out->startDocument('1.0','UTF-8');
        $out->startElement("check_result");
        $out->writeAttribute("status", $status);
        $out->startElement("description");
        $out->text($description);
        $out->endElement();
        $out->startElement("checkoutput");
        $out->text($output);
        $out->endElement();
        $out->endElement();
        return $out->outputMemory(true);

    }

    static function normalize_status($status) {
        switch ($status) {
            case self::UNKNOWN:
            case self::WARNING:
            case self::OK:
                return $status;
        }
        return self::CRITICAL;
    }
}


class local_nagios_exception extends Exception {

    public $status;

    public function __construct ($message, $code = local_nagios_check::CRITICAL) {
        $this->status = local_nagios_check::normalize_status($code);
        parent::__construct($message);
    }

    public function getStatus() {
        return $this->status;
    }
}